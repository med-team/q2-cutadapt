Source: q2-cutadapt
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Liubov Chuprikova <chuprikovalv@gmail.com>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3,
               python3-setuptools,
               python3-pytest-cov,
               python3-pandas,
               cutadapt,
               qiime (>= @DEB_VERSION_UPSTREAM@),
               q2-types (>= @DEB_VERSION_UPSTREAM@)
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/q2-cutadapt
Vcs-Git: https://salsa.debian.org/med-team/q2-cutadapt.git
Homepage: https://qiime2.org/
Rules-Requires-Root: no

Package: q2-cutadapt
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${python3:Depends},
         python3-numpy,
         python3-pandas,
         cutadapt,
         pigz,
         qiime (>= @DEB_VERSION_UPSTREAM@),
         q2-types (>= @DEB_VERSION_UPSTREAM@)
Description: QIIME 2 plugin to work with adapters in sequence data
 QIIME 2 is a powerful, extensible, and decentralized microbiome analysis
 package with a focus on data and analysis transparency. QIIME 2 enables
 researchers to start an analysis with raw DNA sequence data and finish with
 publication-quality figures and statistical results.
 Key features:
  * Integrated and automatic tracking of data provenance
  * Semantic type system
  * Plugin system for extending microbiome analysis functionality
  * Support for multiple types of user interfaces (e.g. API, command line,
 graphical)
 .
 QIIME 2 is a complete redesign and rewrite of the QIIME 1 microbiome analysis
 pipeline. QIIME 2 will address many of the limitations of QIIME 1, while
 retaining the features that makes QIIME 1 a powerful and widely-used analysis
 pipeline.
 .
 QIIME 2 currently supports an initial end-to-end microbiome analysis pipeline.
 New functionality will regularly become available through QIIME 2 plugins. You
 can view a list of plugins that are currently available on the QIIME 2 plugin
 availability page. The future plugins page lists plugins that are being
 developed.
